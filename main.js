let id = null
const makeVariable = (id)=>{
    eval(`const ${id} = document.querySelector("#${id}")`)
}
const input = (id, type='text',label='',placeholder='')=>{
    return `
    <label>${label}</label><br>
    <input type="${type}" id="${id}" placeholder="${placeholder}"><br>
    `
    makeVariable(id)
}
const button = (id,label)=>{
    return`<button id="${id}">${label}</button>`
    makeVariable(id)
}
const div = (id)=>{
    return`<div id="${id}"></div>`
    makeVariable(id)
}
const editData = (index)=>{
    nomormeja.value = data[index].nomor
    menumakanan.value = data[index].menu
    jumlahpesanan.value = data[index].jumlah
    id = index
}
const deleteData = (index)=>{
    data.splice(index,1)
    loadData(data,dataList)
}
const loadData = (data, element)=>{
    element.innerHTML =''
    let i = 0
    data.forEach(item => {
        element.innerHTML += `
        <p>
        Nomor Meja :${item.nomor}<br>
        Menu Makanan :${item.menu}<br> 
        Jumlah makanan :${item.jumlah} <button onclick="editData(${i})">edit</button> 
        <button onclick="deleteData(${i})">Hapus</button> 
        </p>
        `

        i++
    });
}
const clear = ()=>{
    nomormeja.value = null
    menumakanan.value = null
    jumlahpesanan.value = null
    id = null
}
let data = [
    {
        'nomor': '12',
        'menu': 'pizza',
        'jumlah': '2'
    },
    {
        'nomor': '16',
        'menu': 'pizza beef',
        'jumlah': '9' 
    }
]
document.body.innerHTML += input('nomormeja','number','Nomor Meja', 'Masukkan Nomor Meja')
document.body.innerHTML += input('menumakanan','text','Menu Makanan', 'Masukkan pesanan anda')
document.body.innerHTML += input('jumlahpesanan','number','Jumlah makanan', 'Masukkan jumlah makanan')
document.body.innerHTML += button('btnKirim','Kirim')
document.body.innerHTML += button('btnHapus','Hapus')
document.body.innerHTML += div('dataList')
loadData(data, dataList)
btnHapus.addEventListener('click', ()=>{
    clear()
})
btnKirim.addEventListener('click', ()=>{
    if(id == null){
        data.push({
            'nomor': nomormeja.value,
            'menu':menumakanan.value,
            'jumlah':jumlahpesanan.value
        })
        clear()
    }else{
        data[id].nomor = nomormeja.value
        data[id].menu = menumakanan.value
        data[id].jumlah = jumlahpesanan.value
    }
    loadData(data,dataList)
})
